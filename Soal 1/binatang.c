#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <dirent.h>
#include <string.h>
#include <time.h>


// dibuat fungsi untuk melakukan dowload file dari drive yang diberikan
void drive_dowload() {
    pid_t child_pid = fork();
    if (child_pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (child_pid == 0) { 
        char *argv[] = {"wget", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL};
        if (execvp("wget", argv) == -1) {
            perror("execvp");
            exit(EXIT_FAILURE);
        }
    } else { // parent process
        waitpid(child_pid, NULL, 0);
        printf("File di google drive berhasil di dowload!\n");
    }
}

// dilakukan untuk menextract file zip yang sudah didownload dari gdrive sebelumnya.
void grive_unzip() {
    pid_t child_pid = fork();
    if (child_pid == -1) {
        perror("Proses Fork Gagal");
        exit(EXIT_FAILURE);
    } else if (child_pid == 0) { 
        execl("/usr/bin/unzip", "unzip", "binatang.zip", NULL);
        perror("Proses execl gagal");
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
        printf("File berhasil di unzip\n");
    }
}

// dilakukan untuk mendapatkan nama file (.jpg) secara acak dari suatu direktory
char* srand_shift_penjagaan() {
    struct dirent *data_temp;
    DIR *directory_dat = opendir(".");
    if (directory_dat == NULL) {
        printf("gagal saat membuka direktory");
        exit(EXIT_FAILURE);
    }

    int sum = 0;
    while ((data_temp = readdir(directory_dat)) != NULL) {
        if (data_temp->d_type == DT_REG) {
            sum++;
        }
    }

    char **nama_file = malloc(sum * sizeof(char *));
    if (nama_file == NULL) {
        printf("Error malloc");
        exit(EXIT_FAILURE);
    }

    rewinddir(directory_dat);

    // simpan nama dari file kedalam char nama_file
    int i = 0;
    while ((data_temp = readdir(directory_dat)) != NULL) {
        if (data_temp->d_type == DT_REG && strstr(data_temp->d_name, ".jpg")) {
            nama_file[i] = data_temp->d_name;
            i++;
        }
    }
    closedir(directory_dat);
    // Dilakukan Pengambilan secara acak
    srand(time(NULL));
    int acak = rand() % i;

    // mengeluarkan output siapa yang melakukan shift penjagaan
    char* nama_file1 = strdup(nama_file[acak]);
    printf("Hewan Yang Melakukan Shift Penjagaan : %s\n", nama_file1);
    free(nama_file);
    return nama_file1;
}

//Dibuat fungsi untuk membuat 3 direktori dan dilakukan perpindahan file seseuai dengan nama di akhirnya

void create_directories_and_move_files() {
    mkdir("HewanDarat", 0777);
    mkdir("HewanAir", 0777);
    mkdir("HewanAmphibi", 0777);
    
    DIR *dir;
    struct dirent *ent;
    char cwd[1024];
    if ((dir = opendir(getcwd(cwd, sizeof(cwd)))) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (strstr(ent->d_name, "amphibi.jpg") != NULL) {
                char src[1024];
                char dst[1024];
                sprintf(src, "%s/%s", cwd, ent->d_name);
                sprintf(dst, "%s/HewanAmphibi/%s", cwd, ent->d_name);
                rename(src, dst);
            }
            else if (strstr(ent->d_name, "darat.jpg") != NULL) {
                char src[1024];
                char dst[1024];
                sprintf(src, "%s/%s", cwd, ent->d_name);
                sprintf(dst, "%s/HewanDarat/%s", cwd, ent->d_name);
                rename(src, dst);
            }
            else if (strstr(ent->d_name, "air.jpg") != NULL) {
                char src[1024];
                char dst[1024];
                sprintf(src, "%s/%s", cwd, ent->d_name);
                sprintf(dst, "%s/HewanAir/%s", cwd, ent->d_name);
                rename(src, dst);
            }
        }
        closedir(dir);
    }
    else {
        perror("Could not open directory");
        exit(EXIT_FAILURE);
    }
}

// dibuat fungsi zip file yang berfungsi untuk melakukan zip dari direktory yang dibuat
void zip_directory(char* directory, char* zip_file_name) {
    pid_t child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    } else if (child_id == 0) { // child process
        char* argv[] = {"zip", "-r", zip_file_name, directory, NULL};
        execv("/usr/bin/zip", argv);
        exit(EXIT_SUCCESS);
    }
}


void Delete_Folder(char InputString[])
{
  pid_t pid;
  pid = fork();
// dilakukan penghapusan folder
  if (pid == 0) {
    // child process
    execl("/bin/rm", "rm", "-r", InputString, NULL);
    perror("execl error");
    exit(EXIT_FAILURE);
  }
 // Jika terdapat eror dalam pembuatan chill proses
  if (pid < 0)
  {
     perror("fork error");
    exit(EXIT_FAILURE);
  };
}

int main() {
    drive_dowload();
    grive_unzip();
  char* shift_penjaga = srand_shift_penjagaan();
  create_directories_and_move_files();

zip_directory("HewanDarat", "HewanDarat.zip");
zip_directory("HewanAir", "HewanAir.zip");
zip_directory("HewanAmphibi", "HewanAmphibi.zip");

// jika ingin menghapus direktory yang sebelumnya telah didowload
Delete_Folder("binatang.zip");
Delete_Folder("HewanDarat");
Delete_Folder("HewanAir");
Delete_Folder("HewanAmphibi");

printf("Program Berhasil berjalan !!\n");
return 0;
}
