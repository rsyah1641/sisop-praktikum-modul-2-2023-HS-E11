#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>


void run_script_at_time(int hour, int minute, int second, char *route)
{
    pid_t child;
    // dilakukan daemon proses
    // Pembuatan chill proses
    child = fork();

    // jika terjadi eror dalan pembuatan chill proses
    if (child < 0)
    {
        exit(EXIT_FAILURE);
    }
    // jika itu merupakan parent process exit with success
    if (child > 0)
    {
        exit(EXIT_SUCCESS);
    }

// mengubah file mode creasi mask menjadi 0
    umask(0);

// dilakukan detach dari child process dari terminal dengan membuat session baru
    if (setsid() < 0)
    {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0)
    {
        exit(EXIT_FAILURE);
    }

// Close the standar file
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1)
    {
        time_t now = time(NULL);
        struct tm *currentTime = localtime(&now);

        if ((currentTime->tm_hour == hour || hour == -2) &&
            (currentTime->tm_min == minute || minute == -2) &&
            (currentTime->tm_sec == second || second == -2))
        {
            pid_t child_pid = fork();

            if (child_pid < 0)
            {
                exit(EXIT_FAILURE);
            }
            else if (child_pid == 0)
            {
                char *argv[] = {"bash", route, NULL};
                execv("/bin/bash", argv);
            }
        }
        sleep(1);
    }
}



// This function converts a time string to an integer value.
int convert_time_string(const char *input_text)
{
// If the string is "*", it returns -2.
    if (strcmp(input_text, "*") == 0)
    {
        return -2;
    }
    int output = 0;
    while (*input_text != '\0')
    {
// If the string contains non-numeric characters, it returns -1.
        if (*input_text < '0' || *input_text > '9')
        {
            return -1;
        }
        output = output * 10 + (*input_text - '0');
        input_text++;
    }
// Otherwise, it returns the integer value of the string.
    return output;
}

int main(int argc, char *argv[]) {
    // Check if there are enough arguments
    if (argc < 5)
    {
        printf("ERROR: Insufficient arguments\n");
        exit(EXIT_FAILURE);
    }

    // Convert time arguments to integers
    int hour = convert_time_string(argv[1]);
    int minute = convert_time_string(argv[2]);
    int second = convert_time_string(argv[3]);
    char *route = argv[4];

    // Get the absolute path of the script
    char fixed_route[4096];
    char *output_path = realpath(route, fixed_route);

    bool statused = false;
    int lenght_route = strlen(route);

    // Check if time arguments are valid
    if (hour == -1 || hour > 23)
    {
        printf("ERROR: Invalid hour format\n");
        statused = true;
    }
    if (minute == -1 || minute > 59)
    {
        printf("ERROR: Invalid minute format\n");
        statused = true;
    }
    if (second == -1 || second > 59)
    {
        printf("ERROR: Invalid second format\n");
        statused = true;
    }

    // Check if script path is valid
    if (lenght_route < 4 || strcmp(route + lenght_route - 3, ".sh") != 0)
    {
        printf("ERROR: Invalid route format\n");
        statused = true;
    }
    if (access(route, F_OK) == -1 || !output_path)
    {
        printf("ERROR: Path does not exist\n");
        statused = true;
    }

    // Exit program if there are any errors
    if (statused)
    {
        printf("Exit Program\n");
        exit(EXIT_FAILURE);
    }

    // Run the script at the specified time
    run_script_at_time(hour, minute, second, fixed_route);

    return 0;
}

