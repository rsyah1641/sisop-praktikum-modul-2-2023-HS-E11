#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <sys/syslog.h>
#include <sys/wait.h>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <sys/types.h>

  // Lokasi tempat kode berjalan (ubah sesuai lokasi direktory folder dari kode ini di letakkan)
  char *location_Dir = "/home/riansyah/soal-shift-2/";
  
void Delete_Folder(char InputString[])
{
  pid_t pid;
  pid = fork();
// dilakukan penghapusan folder
  if (pid == 0) {
    // child process
    execl("/bin/rm", "rm", "-r", InputString, NULL);
    perror("execl error");
    exit(EXIT_FAILURE);
  }
 // Jika terdapat eror dalam pembuatan chill proses
  if (pid < 0)
  {
     perror("fork error");
    exit(EXIT_FAILURE);
  };
}

void zipDirectory(char InputString[])
{
  pid_t pid = fork();

     // Dilakukan pembuatan ZIP dengan memasukkan semua file dari suatu folder kedalam zip
    char zipPath[100];
    memset(zipPath, 0, sizeof(zipPath));
    const char* dirPath = "../";
    strcat(zipPath, dirPath);
    strcat(zipPath, InputString);
    char* const zipArgv[] = {"zip", "-r", zipPath, ".", NULL};
    
// Jika terdapat eror dalam pembuatan chill proses
  if (pid == 0) {
      if (execv("/usr/bin/zip", zipArgv) == -1) {
            perror("Failed to execute zip command");
            exit(EXIT_FAILURE);
        }
  }
  if (pid < 0)
  {
   perror("Failed to fork process");
  exit(EXIT_FAILURE);
  } else {
    wait(NULL);
  }
}

void Dowload_Gambar_picsum()
{
  pid_t pid = fork();
  if (pid == 0) {

     // membuat folder dan nama file berdasarkan waktu saat ini
        char file_name[100];
        time_t time_now = time(NULL);
        struct tm* timestamp = localtime(&time_now);
        strftime(file_name, sizeof(file_name), "%Y-%m-%d_%H:%M:%S", timestamp);

    // dilakukan dowload gambar dari https://picsum.photos/width dengan ukuran gambar sesuai dengan rumus + waktu
        char download_url[100];
        int file_size = time_now % 1000 + 50;
        snprintf(download_url, sizeof(download_url), "https://picsum.photos/%d", file_size);

        // Digunakan Fungsi wget untuk melakukan dowload gambar dan menyimpan sesuai dengan nama file yang telah ditentukan
          char* wget_argv[] = {"wget", "-q", "-O", file_name, download_url, NULL};
        if (execvp("/usr/bin/wget", wget_argv) == -1) {
            perror("Error: Failed to execute wget command");
            exit(EXIT_FAILURE);
        }
  }
  
  // Jika terdapat eror dalam pembuatan chill proses
  if (pid < 0)
  {
    perror("Error: Failed to create child process");
    exit(EXIT_FAILURE);
  } 
}


int main(int argc, char *argv[]){
char InputString[100];
pid_t chill_id, killer_pid, child_child_proses, chill_PID;
int vix, change_Dir,lok_chill = -1;
  if (argc == 2) {
    if (strcmp (argv[1], "-a") == 0) {
      lok_chill = 0;
    } else if (strcmp (argv[1], "-b") == 0) {
      lok_chill = 1;
    }else if (strcmp (argv[1], "kill") == 0){
         // Melakukan killer pada program yang sudah berjalan dengan fungsi pkill
       char *pkillArgv[] = {"pkill", "lukisan", NULL};
        execv("/usr/bin/pkill", pkillArgv);
    }else{
      printf("Perintah yang dimasukkan tidak sesuai\n");
       exit(EXIT_FAILURE);
    }
  }else{
     printf("Perintah yang dimasukkan tidak sesuai\n");
      exit(EXIT_FAILURE);
  }

// dilakukan daemon proses
// Pembuatan chill proses
chill_id = fork();
// jika terjadi eror dalan pembuatan chill proses
if (chill_id < 0) {
    exit(EXIT_FAILURE);
}
// jika itu merupakan parent process exit with success
if (chill_id > 0) {
    exit(EXIT_SUCCESS);
}
// Detach the child process from the terminal by creating a new session
if (setsid() < 0) {
    exit(EXIT_FAILURE);
}

// Change the current working directory to the specified location
if (chdir(location_Dir) < 0) {
    exit(EXIT_FAILURE);
}

// Close the standard file descriptors
close(STDIN_FILENO);
close(STDOUT_FILENO);
close(STDERR_FILENO);

// Change the file mode creation mask to 0
umask(0);


  // proses untukchill program 
  while (1) {
    // mendapatkan data waktu saat ini
  InputString[100];
        time_t time_now = time(NULL);
        struct tm* timestamp = localtime(&time_now);
        strftime(InputString, sizeof(InputString), "%Y-%m-%d_%H:%M:%S", timestamp);
    // Membuat direktory dengan permision
    mkdir(InputString, 0777);
    
    printf("start child with daemon\n");
    // proses dari childnya child process
    child_child_proses = fork();
    signal(SIGCHLD,SIG_IGN);

    if (child_child_proses < 0) {
      exit(EXIT_FAILURE);
    }

    // if is child child process
    if (child_child_proses == 0) {
      signal(SIGCHLD,SIG_DFL);
      if (lok_chill) {
        prctl(PR_SET_NAME,"dirChld\0",NULL,NULL,NULL);
      }

      printf("dilakukan chill proses\n");
      // mengubah dir
      chdir(InputString);
      
      chill_PID = fork();
      // jika terjadi chill-chill proses
      if (chill_PID == 0) {
         // handle all SIGCHLD requests
        signal(SIGCHLD,SIG_IGN);
        // Dowload gambar dari picsum di setiap 5 detik sebanyak 15 kali
        for (int i = 0; i < 15; i++) {
          Dowload_Gambar_picsum();
          sleep(5);
      }
      // Jika terdapat error, maka dilakukan exit
      if (chill_PID < 0) {
        exit(EXIT_FAILURE);
        }
      } else {
        wait(NULL);
      }
      // jika sudah berhasil dowload semua file dalam folder
      // lalu dilakukan ZIP dan penghapusan folder
      zipDirectory(InputString);
      printf("*** zipped Berhasil ***\n");
      chdir("..");
      Delete_Folder(InputString);
      printf("*** Folder Berhasil di hapus ***\n");
      exit(EXIT_SUCCESS);
    }
    sleep(30);
  }
}
