#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>


// dibuat fungsi untuk melakukan dowload file dari drive yang diberikan
void drive_dowload() {
    pid_t child_pid = fork();
    if (child_pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (child_pid == 0) { 
        char *argv[] = {"wget", "-O", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};
        if (execvp("wget", argv) == -1) {
            perror("execvp");
            exit(EXIT_FAILURE);
        }
    } else { // parent process
        waitpid(child_pid, NULL, 0);
        printf("File di google drive berhasil di dowload!\n");
    }
}


// dilakukan untuk menextract file zip yang sudah didownload dari gdrive sebelumnya.
void grive_unzip() {
    pid_t child_pid = fork();
    if (child_pid == -1) {
        perror("Proses Fork Gagal");
        exit(EXIT_FAILURE);
    } else if (child_pid == 0) { 
        execl("/usr/bin/unzip", "unzip", "players.zip", NULL);
        perror("Proses execl gagal");
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
        printf("File berhasil di unzip\n");
    }
}

// Fungsi untuk menghapus semua tim selain dari MU
void remove_selain_MU(){
    pid_t child_id = fork();

    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *args[] = {"find", "players", "-type", "f", "!", "-name", "*_ManUtd_*", "-exec", "rm", "{}", "+", NULL};
        execvp("find", args);
    }
    else {
        int status;
        while(wait(&status) > 0);
    }
}

//Pembuatan penyimpanan direktori untuk menyimpan file data yang didownload
void create_directories() {
    mkdir("./players/penyerang", 0777);
    mkdir("./players/bek", 0777);
    mkdir("./players/gelandang", 0777);
    mkdir("./players/kiper", 0777);
}

// memasukkan setiap file kedalam folder yang sudah dipersiapkan berdasarkan posisi
void filter_file_to_folder(const char* keyword, const char* folder_name) {
    char* argv[] = {"find", "players", "-name", keyword, "-type", "f", "-exec", "cp", "{}", folder_name, ";", NULL};
    execv("/usr/bin/find", argv);
}

// Program dibuat untuk melakukan eksekusi execv pada fungsi Create_Tim_Bola
void execute(char* command, char** args) {
    pid_t pid = fork();

    if (pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        if (execv(command, args) < 0) {
            perror("execv");
            exit(EXIT_FAILURE);
        }
    } else {
        int status;
        while (wait(&status) > 0);
    }
}

// fungsi dibuat untuk menentukan Tim Kesebelasan Terbaik untuk tim di memou MU
void Create_Tim_Bola(int defense, int midfield, int attack) {
    // mendefinisikan posisi di tim
    struct Position {
        const char* name;
        const char* directory;
        int count;
    } positions[] = {
        { "Kiper", "kiper", 1 },
        { "Bek", "bek", defense },
        { "gelandang", "gelandang", midfield },
        { "penyerang", "penyerang", attack }
    };

    // Membuat suatu file untuk setiap posisi dan menulis list dari player
    for (int i = 0; i < 4; i++) {
        char* args[] = { "/bin/sh", "-c", NULL, NULL };
        asprintf(&args[2], "ls ./players/%s > %s.txt", positions[i].directory, positions[i].name);
        execute(args[0], args);
        free(args[2]);
    }

    // Melakukan sortir dari list pemain untuk setiap posisi
    for (int i = 0; i < 4; i++) {
        char* args[] = { "/bin/sort", "-t_", "-k", "3", "-r", NULL, "-o", NULL };
        asprintf(&args[5], "%s.txt", positions[i].name);
        asprintf(&args[7], "%s.txt", positions[i].name);
        execute(args[0], args);
        free(args[5]);
        free(args[7]);
    }

    // Memilih pemain terbaik untuk setiap pisisi dan disimpan di suatu file
    char filename[100];
    //direktory harus diganti terlebih dahulu pada folder code yang disimpan
    snprintf(filename, sizeof(filename), "/home/riansyah/soal-shift-2/Formasi_%d_%d_%d.txt", defense, midfield, attack);

    for (int i = 3; i >= 0; i--) {
        char* args[] = { "/bin/sh", "-c", NULL, NULL };
        asprintf(&args[2], "head -n %d %s.txt >> %s", positions[i].count, positions[i].name, filename);
        execute(args[0], args);
        free(args[2]);
    }
}

// Melakukan penghapusan dari players.zip yag sudah didownload
void Remove_zip(char InputString[])
{
  pid_t pid;
  pid = fork();
// dilakukan penghapusan folder
  if (pid == 0) {
    // child process
    execl("/bin/rm", "rm", "-r", InputString, NULL);
    perror("execl error");
    exit(EXIT_FAILURE);
  }
 // Jika terdapat eror dalam pembuatan chill proses
  if (pid < 0)
  {
     perror("fork error");
    exit(EXIT_FAILURE);
  };
}

int main(){
    drive_dowload();
    grive_unzip();
    create_directories();   
    remove_selain_MU();

    pid_t child_id;
    int status;
  const char* keywords[] = {"*Penyerang*", "*Gelandang*", "*Bek*", "*Kiper*"};
    const char* folder_names[] = {"./players/penyerang", "./players/gelandang", "./players/bek", "./players/kiper"};
    
for (int i = 0; i < 4; i++) {
        child_id = fork();

        if (child_id < 0) {
            fprintf(stderr, "Failed to create child process %d\n", i);
            exit(EXIT_FAILURE);
        }
        else if (child_id == 0) {
            filter_file_to_folder(keywords[i], folder_names[i]);
        }
    }

    for (int i = 0; i < 4; i++) {
        wait(&status);
    }
    Create_Tim_Bola(4, 3, 3);
    Remove_zip("players.zip");
    return 0;
}
