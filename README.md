# sisop-praktikum-modul-1-2023-HS-E11



## Anggota Kelompok E11
| Nama                      | NRP        |
|---------------------------|------------|
| Muhammad Febriansyah      | 5025211164 |
| Sastiara Maulikh          | 5025201257 |
| Schaquille Devlin Aristano| 5025211211 | 

#Instalation
Untuk Menjalankan program ini kira perlu untuk mengistall beberapa package terlebih dahulu ada wget = (untuk melakukan dowload dari URL), Unzip dan Zip , GCC (untuk Menjalankan program)

sintax untuk menginstall wget
```sh
sudo apt install wget

```

sintax untuk mengistall unzip dan zip
```sh
sudo apt install unzip
sudo apt install zip

```

sintax untuk mengistall GCC
```sh
sudo apt-get install gcc

```



# Soal 1
Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 

# Catatan
- untuk melakukan zip dan unzip tidak boleh menggunakan system

## Soal 1 bagian 1
Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.


## Cara Penyelesaian

program untuk melakukan download file dari Google Drive,kemudian meng-unzip file, mula-mula dibuat program untuk melakukan dowload dari Google Drive dengan menggunakan command ```wget``` pada Linux. Pada fungsi ini dilakukan ```fork()``` untuk membuat child process dan melakukan download pada child process tersebut. Jika proses download telah selesai, maka parent process akan menunggu child process tersebut dengan menggunakan ```waitpid()```.

## Source Code
```sh

// dibuat fungsi untuk melakukan dowload file dari drive yang diberikan
void drive_dowload() {
    pid_t child_pid = fork();
    if (child_pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (child_pid == 0) { 
        char *argv[] = {"wget", "-O", "binatang.zip", "https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", NULL};
        if (execvp("wget", argv) == -1) {
            perror("execvp");
            exit(EXIT_FAILURE);
        }
    } else { // parent process
        waitpid(child_pid, NULL, 0);
        printf("File di google drive berhasil di dowload!\n");
    }
}


// dilakukan untuk menextract file zip yang sudah didownload dari gdrive sebelumnya.
void grive_unzip() {
    pid_t child_pid = fork();
    if (child_pid == -1) {
        perror("Proses Fork Gagal");
        exit(EXIT_FAILURE);
    } else if (child_pid == 0) { 
        execl("/usr/bin/unzip", "unzip", "binatang.zip", NULL);
        perror("Proses execl gagal");
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
        printf("File berhasil di unzip\n");
    }
}

```

# Soal 1 bagian 2
Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

### Cara Penyelesaian
Dibuat suatu program yang berguna untuk mendapatkan nama file acak yang berupa file gambar (.jpg) dari direktori. Pertama, fungsi ini akan membuka suatu direktori dengan menggunakan ```opendir()```. Kemudian, fungsi akan melakukan loop pada setiap file yang ada di direktori tersebut dan akan menyimpan nama file yang memiliki ekstensi ".jpg" ke dalam array ```doct[]```. Setelah itu, fungsi akan men-generate angka acak untuk memilih nama file yang akan dipilih. Terakhir, fungsi akan mengembalikan nama file yang dipilih secara acak.

### Source Code
```sh

// dilakukan untuk mendapatkan nama file (.jpg) secara acak dari suatu direktory
char* srand_shift_penjagaan() {
    struct dirent *data_temp;
    DIR *directory_dat = opendir(".");
    if (directory_dat == NULL) {
        printf("gagal saat membuka direktory");
        exit(EXIT_FAILURE);
    }

    int sum = 0;
    while ((data_temp = readdir(directory_dat)) != NULL) {
        if (data_temp->d_type == DT_REG) {
            sum++;
        }
    }

    char **nama_file = malloc(sum * sizeof(char *));
    if (nama_file == NULL) {
        printf("Error malloc");
        exit(EXIT_FAILURE);
    }

    rewinddir(directory_dat);

    // simpan nama dari file kedalam char nama_file
    int i = 0;
    while ((data_temp = readdir(directory_dat)) != NULL) {
        if (data_temp->d_type == DT_REG && strstr(data_temp->d_name, ".jpg")) {
            nama_file[i] = data_temp->d_name;
            i++;
        }
    }
    closedir(directory_dat);
    // Dilakukan Pengambilan secara acak
    srand(time(NULL));
    int acak = rand() % i;

    // mengeluarkan output siapa yang melakukan shift penjagaan
    char* nama_file1 = strdup(nama_file[acak]);
    printf("Hewan Yang Melakukan Shift Penjagaan : %s\n", nama_file1);
    free(nama_file);
    return nama_file1;
}

```


## soal 1 bagian 3
Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

### Cara Penyelesaian
Dibuat suatu program yang berguna untuk membuat direktori baru untuk setiap direktory digolongkan berdasarkan jenis hewan dan memindahkan gambar ke direktori yang sesuai. Pada fungsi ini, dilakukan pembuatan direktori baru menggunakan command ````mkdir``` pada Linux. Kemudian, dilakukan pemindahan gambar ke direktori yang sesuai dengan menggunakan command ```mv``` pada sistem Linux.

### Source Code
```sh

//Dibuat fungsi untuk membuat 3 direktori dan dilakukan perpindahan file seseuai dengan nama di akhirnya

void create_directories_and_move_files() {
    mkdir("HewanDarat", 0777);
    mkdir("HewanAir", 0777);
    mkdir("HewanAmphibi", 0777);
    
    DIR *dir;
    struct dirent *ent;
    char cwd[1024];
    if ((dir = opendir(getcwd(cwd, sizeof(cwd)))) != NULL) {
        while ((ent = readdir(dir)) != NULL) {
            if (strstr(ent->d_name, "amphibi.jpg") != NULL) {
                char src[1024];
                char dst[1024];
                sprintf(src, "%s/%s", cwd, ent->d_name);
                sprintf(dst, "%s/HewanAmphibi/%s", cwd, ent->d_name);
                rename(src, dst);
            }
            else if (strstr(ent->d_name, "darat.jpg") != NULL) {
                char src[1024];
                char dst[1024];
                sprintf(src, "%s/%s", cwd, ent->d_name);
                sprintf(dst, "%s/HewanDarat/%s", cwd, ent->d_name);
                rename(src, dst);
            }
            else if (strstr(ent->d_name, "air.jpg") != NULL) {
                char src[1024];
                char dst[1024];
                sprintf(src, "%s/%s", cwd, ent->d_name);
                sprintf(dst, "%s/HewanAir/%s", cwd, ent->d_name);
                rename(src, dst);
            }
        }
        closedir(dir);
    }
    else {
        perror("Could not open directory");
        exit(EXIT_FAILURE);
    }
}

```

## Soal 1 bagian 4
Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan

### Cara Penyelesaian
Dibuat suatu Fungsi yang berguna untuk melakukan zip pada direktori yang telah dibuat sebelumnya. Fungsi ini juga dilakukan ```fork()``` untuk membuat child process dan melakukan zip pada child process tersebut dengan menggunakan command ```zip``` pada Linux. Lalu dilakukan penghapusan file dengan rm pada direktory yg dibuat sebelumnya sehingga hanya menyisakan file zip saja.

### Source Kode
```sh
// dibuat fungsi zip file yang berfungsi untuk melakukan zip dari direktory yang dibuat
void zip_directory(char* directory, char* zip_file_name) {
    pid_t child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    } else if (child_id == 0) { // child process
        char* argv[] = {"zip", "-r", zip_file_name, directory, NULL};
        execv("/usr/bin/zip", argv);
        exit(EXIT_SUCCESS);
    }
}

```

### penghapusan folder
dibuat fungsi untuk menghapus folder serta dilakukan penghapusan folder seletah folder berhasil di zip

```sh

void Delete_Folder(char InputString[])
{
  pid_t pid;
  pid = fork();
// dilakukan penghapusan folder
  if (pid == 0) {
    // child process
    execl("/bin/rm", "rm", "-r", InputString, NULL);
    perror("execl error");
    exit(EXIT_FAILURE);
  }
 // Jika terdapat eror dalam pembuatan chill proses
  if (pid < 0)
  {
     perror("fork error");
    exit(EXIT_FAILURE);
  };
}

// pada int main()
int main(){
// jika ingin menghapus direktory yang sebelumnya telah didowload
Delete_Folder("binatang.zip");
Delete_Folder("HewanDarat");
Delete_Folder("HewanAir");
Delete_Folder("HewanAmphibi");
}
```
### Hasil Output
cara run dan output akhir
![image](http://itspreneur.com/wp-content/uploads/2023/03/Screenshot-1337-e1679714801484.png)

output sebelum dilakukan penghapusan pada folder yang belum dibuat zip dan siapa yang melakukan shift penjagaan
![image](http://itspreneur.com/wp-content/uploads/2023/03/Screenshot-1339-e1679715004580.png)

# Soal 2
Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

#catatan
- Tidak boleh menggunakan system()
- Proses berjalan secara daemon
- Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

### inisiasi Variable
Menginisialisasi variabel-variabel yang akan digunakan, seperti location_Dir yang akan menjadi lokasi direktori dari kode ini diletakkan, pid_t yang digunakan untuk membuat chill proses, InputString yang akan digunakan sebagai nama folder yang akan dibuat. 

```sh
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <sys/syslog.h>
#include <sys/wait.h>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <sys/types.h>

char *location_Dir = "/home/riansyah/soal-shift-2/";
char InputString[100];
pid_t chill_id, killer_pid, child_child_proses, chill_PID;
int vix, change_Dir,lok_chill=0;

```

### Soal 2 bagian 1
Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

### Cara Penyelesaian
pada ```int main()``` dilakukan lopping secara terus menerus dan di siapkan char untuk menyimpan hasil waktu saat ini sebagai nama file dengan menggunakan ```strftime```, kemudian dibuat folder dengan ```mkdir``` dengan penamaan dari char yg didapat dan permision 0 7 7 7

### Source Code
```sh
 while (1) {
    // mendapatkan data waktu saat ini
  InputString[100];
        time_t time_now = time(NULL);
        struct tm* timestamp = localtime(&time_now);
        strftime(InputString, sizeof(InputString), "%Y-%m-%d_%H:%M:%S", timestamp);
    // Membuat direktory dengan permision
    mkdir(InputString, 0777);

```

## Soal 2 bagian 2
Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss

### Cara Penyelesaian
Dibuat suatu fungsi untuk melakukan dowload dari link picsum.photos dengan menggunakan char untuk menyimpan data waktu saat ini yang nantinya digunakan untuk nama file yang diperoleh dari link picsum.photos dengan menggunkanan ```wget```.lalu pada int main dilakukan looping sebanyak 15 kali untuk memanggil fungsi tersebut dengan jeda waktu sleep(5) detik.

### Source Code
``` sh

void Dowload_Gambar_picsum()
{
  pid_t pid = fork();
  if (pid == 0) {

     // membuat folder dan nama file berdasarkan waktu saat ini
        char file_name[100];
        time_t time_now = time(NULL);
        struct tm* timestamp = localtime(&time_now);
        strftime(file_name, sizeof(file_name), "%Y-%m-%d_%H:%M:%S", timestamp);

    // dilakukan dowload gambar dari https://picsum.photos/width dengan ukuran gambar sesuai dengan rumus + waktu
        char download_url[100];
        int file_size = time_now % 1000 + 50;
        snprintf(download_url, sizeof(download_url), "https://picsum.photos/%d", file_size);

        // Digunakan Fungsi wget untuk melakukan dowload gambar dan menyimpan sesuai dengan nama file yang telah ditentukan
          char* wget_argv[] = {"wget", "-q", "-O", file_name, download_url, NULL};
        if (execvp("/usr/bin/wget", wget_argv) == -1) {
            perror("Error: Failed to execute wget command");
            exit(EXIT_FAILURE);
        }
  }
  
  // Jika terdapat eror dalam pembuatan chill proses
  if (pid < 0)
  {
    perror("Error: Failed to create child process");
    exit(EXIT_FAILURE);
  } 
}

// pada int main()
int main(){
     printf("dilakukan chill proses\n");
      // mengubah dir
      chdir(InputString);
      
      pid_t chill_PID = fork();
      // jika terjadi chill-chill proses
      if (chill_PID == 0) {
         // handle all SIGCHLD requests
        signal(SIGCHLD,SIG_IGN);
        // Dowload gambar dari picsum di setiap 5 detik sebanyak 15 kali
        for (int i = 0; i < 15; i++) {
          Dowload_Gambar_picsum();
          sleep(5);
      }
      // Jika terdapat error, maka dilakukan exit
      if (chill_PID < 0) {
        exit(EXIT_FAILURE);
        }
      } else {
        wait(NULL);
      }
}

```

## Soal 2 bagian 3
Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

### Cara Penyelesaian
Dibuat 2 fungsi yaitu delete_folder guna untuk menghapus folder yang telah dibuat dengan menggunakan execl. dan fungsi zip_directory guna untuk membuat zip dari folder sebelum dilakukan penghapusan. kemudian dilakukan pemanggilan di int main() dari kedua fungsi tersebut berdasarkan nama_file yang diperoleh dari timestamp.

### Source Code
``` sh

void Delete_Folder(char InputString[])
{
  pid_t pid;
  pid = fork();
// dilakukan penghapusan folder
  if (pid == 0) {
    // child process
    execl("/bin/rm", "rm", "-r", InputString, NULL);
    perror("execl error");
    exit(EXIT_FAILURE);
  }
 // Jika terdapat eror dalam pembuatan chill proses
  if (pid < 0)
  {
     perror("fork error");
    exit(EXIT_FAILURE);
  };
}

void zipDirectory(char InputString[])
{
  pid_t pid = fork();

     // Dilakukan pembuatan ZIP dengan memasukkan semua file dari suatu folder kedalam zip
    char zipPath[100];
    memset(zipPath, 0, sizeof(zipPath));
    const char* dirPath = "../";
    strcat(zipPath, dirPath);
    strcat(zipPath, InputString);
    char* const zipArgv[] = {"zip", "-r", zipPath, ".", NULL};
    
// Jika terdapat eror dalam pembuatan chill proses
  if (pid == 0) {
      if (execv("/usr/bin/zip", zipArgv) == -1) {
            perror("Failed to execute zip command");
            exit(EXIT_FAILURE);
        }
  }
  if (pid < 0)
  {
   perror("Failed to fork process");
  exit(EXIT_FAILURE);
  } else {
    wait(NULL);
  }
}

// pada fungsi int main()
int main(){
      zipDirectory(InputString);
      printf("*** zipped Berhasil ***\n");
      chdir("..");
      Delete_Folder(InputString);
      printf("*** Folder Berhasil di hapus ***\n");
      exit(EXIT_SUCCESS);
}
```

## Soal 2 bagian 4
Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

### Cara Penyelesaian
Dilakukan deteksi input dengan argv dengan jenis inputan "-a", "-b" , "kill". jika program sudah di GCC maka perintah kill ini dapat digunakan dengan format ./[nama_gcc] kill . untuk proses killnya sendiri digunakan fungsi ```pkill``` & ```execv``` untuk menghentikan semua proses yang bernama lukisan dan menjalankan program tersebut.

### Source Code
``` sh
  if (argc == 2) {
    if (strcmp (argv[1], "-a") == 0) {
      lok_chill = 0;
    } else if (strcmp (argv[1], "-b") == 0) {
      lok_chill = 1;
    }else if (strcmp (argv[1], "kill") == 0){
         // Melakukan killer pada program yang sudah berjalan dengan fungsi pkill
       char *pkillArgv[] = {"pkill", "lukisan", NULL};
        execv("/usr/bin/pkill", pkillArgv);
    }else{
      printf("Perintah yang dimasukkan tidak sesuai\n");
       exit(EXIT_FAILURE);
    }
  }else{
     printf("Perintah yang dimasukkan tidak sesuai\n");
      exit(EXIT_FAILURE);
  }

```

## Soal 2 bagian 5
Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).

### Cara Penyelesaian
dibuat fungsi argc pada int main(), seperti diatas untuk mendeteksi inputan "-a" , dan "-b" dengan. jika dilakukan perintah "-a" maka ketika dilakukan kill proses maka proses akan langsung berhenti. sedangkan jika dilakukan perintah "-b" maka akan dilakukan perintah ```prctl(PR_SET_NAME,"dirChld\0",NULL,NULL,NULL);```  guna untuk mengatur/mengkontrol suatu proses maka sehingga ketika di kill proses tetap berjalan hingga selesai untuk melakukan zip pada suatu folder  

### Source Code
``` sh
if (argc == 2) {
    if (strcmp (argv[1], "-a") == 0) {
      lok_chill = 0;
    } else if (strcmp (argv[1], "-b") == 0) {
      lok_chill = 1;
    }else if (strcmp (argv[1], "kill") == 0){
         // Melakukan killer pada program yang sudah berjalan dengan fungsi pkill
       char *pkillArgv[] = {"pkill", "lukisan", NULL};
        execv("/usr/bin/pkill", pkillArgv);
    }else{
      printf("Perintah yang dimasukkan tidak sesuai\n");
       exit(EXIT_FAILURE);
    }
  }else{
     printf("Perintah yang dimasukkan tidak sesuai\n");
      exit(EXIT_FAILURE);
  }

  
// dilakukan daemon proses
// Pembuatan chill proses
chill_id = fork();
// jika terjadi eror dalan pembuatan chill proses
if (chill_id < 0) {
    exit(EXIT_FAILURE);
}
// jika itu merupakan parent process exit with success
if (chill_id > 0) {
    exit(EXIT_SUCCESS);
}
// Detach the child process from the terminal by creating a new session
if (setsid() < 0) {
    exit(EXIT_FAILURE);
}

// Change the current working directory to the specified location
if (chdir(location_Dir) < 0) {
    exit(EXIT_FAILURE);
}

// Close the standard file descriptors
close(STDIN_FILENO);
close(STDOUT_FILENO);
close(STDERR_FILENO);

// Change the file mode creation mask to 0
umask(0);



// pada proses child-child

    // if is child child process
    if (child_child_proses == 0) {
      signal(SIGCHLD,SIG_DFL);
      if (lok_chill) {
        prctl(PR_SET_NAME,"dirChld\0",NULL,NULL,NULL);
      }

      printf("dilakukan chill proses\n");
      // mengubah dir
      chdir(InputString);

```

### Hasil Output
Ketika dijalankan dengan menggunakan perintah -a dan gunakan perintah kill untuk menghentikan program
![image](http://itspreneur.com/wp-content/uploads/2023/03/Screenshot-1340-e1679715825756.png)
ketika di kill program ada langsung berhenti, dan tidak ada proses yang berjalan. untuk setiap 30 detik akan dibuat folder baru dan untuk setiap 5 detik akan dilakukan dowload dengan format timestamp


Ketika dijalankan dengan menggunakan perintah -b lalu digunakan perintah kill untuk menghentikan program maka program masih tetap berjalan dapat dilihat melalui ```ps aux```
![image](http://itspreneur.com/wp-content/uploads/2023/03/Screenshot-1343-e1679722833933.png)

Kemudian setelah proses sudah selesai hingga folder berhasil di zip dan dihapus maka proses akan berhenti
![image](http://itspreneur.com/wp-content/uploads/2023/03/Screenshot-1344-e1679722979559.png)


# Soal 3
Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”

# Catatan
- Format nama file yang akan diunduh dalam zip dan isi txt formasi berupa [nama]_[tim]_[posisi]_[rating].png
- Tidak boleh menggunakan system()
- Tidak boleh memakai function C mkdir() ataupun rename().
- Gunakan exec() dan fork().
- Directory “.” dan “..” tidak termasuk yang akan dihapus.
- Untuk poin d DIWAJIBKAN membuat fungsi bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker.



## Soal 3 bagian 1
Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.


### Cara Penyelesaian
dibuat dua fungsi untuk melalukan dowload dari gdrive dan melakukan ekstrak file dengan menggunakan ```fork``` dan menggunakan eksekusi pada ```wget```  dan fungsi ```unzip``` untuk melakukan unzip file.

### Source Code
```sh
// dibuat fungsi untuk melakukan dowload file dari drive yang diberikan
void drive_dowload() {
    pid_t child_pid = fork();
    if (child_pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (child_pid == 0) { 
        char *argv[] = {"wget", "-O", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};
        if (execvp("wget", argv) == -1) {
            perror("execvp");
            exit(EXIT_FAILURE);
        }
    } else { // parent process
        waitpid(child_pid, NULL, 0);
        printf("File di google drive berhasil di dowload!\n");
    }
}


// dilakukan untuk menextract file zip yang sudah didownload dari gdrive sebelumnya.
void grive_unzip() {
    pid_t child_pid = fork();
    if (child_pid == -1) {
        perror("Proses Fork Gagal");
        exit(EXIT_FAILURE);
    } else if (child_pid == 0) { 
        execl("/usr/bin/unzip", "unzip", "players.zip", NULL);
        perror("Proses execl gagal");
        exit(EXIT_FAILURE);
    } else {
        wait(NULL);
        printf("File berhasil di unzip\n");
    }
}

```

## Soal 3 bagian 2
Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.  

### Cara Penyelesaian
Dibuat fungsi remove_selain_MU() untuk menghapus semua file yang bukan dari tim MU dari file yang telah di dowload pada gdrive degnan menggunakan ```fork``` dan ```execv``` untuk menjalaknkan program

### Source Code
```sh

// Fungsi untuk menghapus semua tim selain dari MU
void remove_selain_MU(){
    pid_t child_id = fork();

    if(child_id < 0){
        exit(EXIT_FAILURE);
    }
    else if (child_id == 0){
        char *args[] = {"find", "players", "-type", "f", "!", "-name", "*_ManUtd_*", "-exec", "rm", "{}", "+", NULL};
        execvp("find", args);
    }
    else {
        int status;
        while(wait(&status) > 0);
    }
}

```

## Soal 3 bagian 3
Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang. 

### Cara Penyelesaian
dibuat fungsi void filter_file_to_folder(const char* keyword, const char* folder_name) yang digunakan untuk memasukkan suatu file dengan syarat tertentu kedalam folder tertentu. lalu dilakukan ```execv``` untuk menjalankan program. 

pada int main juga dilakukan loping sebanyak 4 kali dengan list keyword {Penyerang, Bek, Kiper, Gelandang}

### Source Code
```sh
// memasukkan setiap file kedalam folder yang sudah dipersiapkan berdasarkan posisi
void filter_file_to_folder(const char* keyword, const char* folder_name) {
    char* argv[] = {"find", "players", "-name", keyword, "-type", "f", "-exec", "cp", "{}", folder_name, ";", NULL};
    execv("/usr/bin/find", argv);
}

// pada int main()
int main(){
 pid_t child_id;
    int status;
  const char* keywords[] = {"*Penyerang*", "*Gelandang*", "*Bek*", "*Kiper*"};
    const char* folder_names[] = {"./players/penyerang", "./players/gelandang", "./players/bek", "./players/kiper"};
    
for (int i = 0; i < 4; i++) {
        child_id = fork();

        if (child_id < 0) {
            fprintf(stderr, "Failed to create child process %d\n", i);
            exit(EXIT_FAILURE);
        }
        else if (child_id == 0) {
            filter_file_to_folder(keywords[i], folder_names[i]);
        }
    }

    for (int i = 0; i < 4; i++) {
        wait(&status);
    }
}

```

## Soal 3 bagian 4
Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/

### Cara Penyelesaian
Dibuat fungsi execute() untuk mengeksekusi program eksternal yaitu ```execv```  yang digunakan pada fungsi Create_Tim_Bola() yang digunakan untuk menentukan tim sepak bola terbaik untuk dengan parameter tertentu. Itu membuat file untuk setiap posisi dalam tim dan menulis daftar pemain ke setiap file. Kemudian mengurutkan daftar pemain untuk setiap posisi dan memilih pemain terbaik untuk setiap posisi. lalu hasilnya disimpan dalam file .txt.

### Source Code
```sh

// Program dibuat untuk melakukan eksekusi execv pada fungsi Create_Tim_Bola
void execute(char* command, char** args) {
    pid_t pid = fork();

    if (pid < 0) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        if (execv(command, args) < 0) {
            perror("execv");
            exit(EXIT_FAILURE);
        }
    } else {
        int status;
        while (wait(&status) > 0);
    }
}

// fungsi dibuat untuk menentukan Tim Kesebelasan Terbaik untuk tim di memou MU
void Create_Tim_Bola(int defense, int midfield, int attack) {
    // mendefinisikan posisi di tim
    struct Position {
        const char* name;
        const char* directory;
        int count;
    } positions[] = {
        { "Kiper", "kiper", 1 },
        { "Bek", "bek", defense },
        { "gelandang", "gelandang", midfield },
        { "penyerang", "penyerang", attack }
    };

    // Membuat suatu file untuk setiap posisi dan menulis list dari player
    for (int i = 0; i < 4; i++) {
        char* args[] = { "/bin/sh", "-c", NULL, NULL };
        asprintf(&args[2], "ls ./players/%s > %s.txt", positions[i].directory, positions[i].name);
        execute(args[0], args);
        free(args[2]);
    }

    // Melakukan sortir dari list pemain untuk setiap posisi
    for (int i = 0; i < 4; i++) {
        char* args[] = { "/bin/sort", "-t_", "-k", "3", "-r", NULL, "-o", NULL };
        asprintf(&args[5], "%s.txt", positions[i].name);
        asprintf(&args[7], "%s.txt", positions[i].name);
        execute(args[0], args);
        free(args[5]);
        free(args[7]);
    }

    // Memilih pemain terbaik untuk setiap pisisi dan disimpan di suatu file
    char filename[100];
    //direktory harus diganti terlebih dahulu pada folder code yang disimpan
    snprintf(filename, sizeof(filename), "/home/riansyah/soal-shift-2/Formasi_%d_%d_%d.txt", defense, midfield, attack);

    for (int i = 3; i >= 0; i--) {
        char* args[] = { "/bin/sh", "-c", NULL, NULL };
        asprintf(&args[2], "head -n %d %s.txt >> %s", positions[i].count, positions[i].name, filename);
        execute(args[0], args);
        free(args[2]);
    }
}

```

## output program soal 3
hasil pengelompokan folder berdasarkan posisi yang disumpan pada folder players setelah dirun programnya 
![image](http://itspreneur.com/wp-content/uploads/2023/03/Screenshot-1345-e1679755202130.png)

Hasil penentuan tim kesebelasan terbaik untuk TIM MU
![image](http://itspreneur.com/wp-content/uploads/2023/03/Screenshot-1346-e1679755464748.png)

# Soal 4
Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.


Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:

- Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.
- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.
- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.
Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.
- Bonus poin apabila CPU state minimum.

Contoh untuk run: /program \* 44 5 /home/Banabil/programcron.sh

### Fungsi run_script_at_time()
Fungsi Ini digunakan untuk menjadwalkan sebuah script untuk dijalankan pada waktu tertentu dalam sehari. Fungsi Ini Dapat Dimodifikasi dengan mengubah nilai dari hour, minute, second, dan route.

Fungsi ini membuat sebuah child process menggunakan fork() dan kemudian melepaskannya dari terminal dengan membuat session baru menggunakan setsid(). Kemudian, ia mengubah working directory-nya ke root directory menggunakan chdir(“/”). Setelah itu, ia menutup semua standard file descriptors menggunakan close(STDIN_FILENO), close(STDOUT_FILENO), dan close(STDERR_FILENO).

Fungsi ini kemudian memasuki infinite loop dimana ia mengecek apakah waktu saat ini sesuai dengan waktu yang ditentukan oleh hour, minute, dan second. Jika sesuai, ia membuat child process lain menggunakan fork() dan menjalankan sebuah bash script yang ditentukan oleh route menggunakan execv().

### Fungsi Convert_time_string
Fungsi Ini Bertujuan Untuk mengonversi sebuah string waktu menjadi nilai integer.

Fungsi ini mengambil argumen yaitu input_text yang merupakan pointer ke string waktu yang akan dikonversi.kemudian Diperiksa apakah string tersebut valid atau tidak. Jika string tersebut valid, maka fungsi ini akan mengembalikan nilai integer yang merepresentasikan waktu tersebut.

Jika string tersebut tidak valid, maka fungsi ini akan mengembalikan nilai -1 jika string tersebut mengandung karakter non-numerik atau -2 jika string tersebut adalah “*”.

#### Int Main () 
Program Ini Dibuat Bertujuan Untuk memeriksa apakah argumen waktu yang diberikan valid dan apakah argumen jalur skrip valid. Jika argumen tidak valid, program akan menampilkan pesan kesalahan dan keluar dengan kode kesalahan. 

Untuk Menjalankan Program

- Ketik perintah “g++ program.cpp -o program” untuk mengkompilasi program.
- Ketik perintah “./program [jam] [menit] [detik] [jalur skrip]” untuk menjalankan program.


### source code
```sh
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>


void run_script_at_time(int hour, int minute, int second, char *route)
{
    pid_t child;
    // dilakukan daemon proses
    // Pembuatan chill proses
    child = fork();

    // jika terjadi eror dalan pembuatan chill proses
    if (child < 0)
    {
        exit(EXIT_FAILURE);
    }
    // jika itu merupakan parent process exit with success
    if (child > 0)
    {
        exit(EXIT_SUCCESS);
    }

// mengubah file mode creasi mask menjadi 0
    umask(0);

// dilakukan detach dari child process dari terminal dengan membuat session baru
    if (setsid() < 0)
    {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/")) < 0)
    {
        exit(EXIT_FAILURE);
    }

// Close the standar file
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while (1)
    {
        time_t now = time(NULL);
        struct tm *currentTime = localtime(&now);

        if ((currentTime->tm_hour == hour || hour == -2) &&
            (currentTime->tm_min == minute || minute == -2) &&
            (currentTime->tm_sec == second || second == -2))
        {
            pid_t child_pid = fork();

            if (child_pid < 0)
            {
                exit(EXIT_FAILURE);
            }
            else if (child_pid == 0)
            {
                char *argv[] = {"bash", route, NULL};
                execv("/bin/bash", argv);
            }
        }
        sleep(1);
    }
}



// This function converts a time string to an integer value.
int convert_time_string(const char *input_text)
{
// If the string is "*", it returns -2.
    if (strcmp(input_text, "*") == 0)
    {
        return -2;
    }
    int output = 0;
    while (*input_text != '\0')
    {
// If the string contains non-numeric characters, it returns -1.
        if (*input_text < '0' || *input_text > '9')
        {
            return -1;
        }
        output = output * 10 + (*input_text - '0');
        input_text++;
    }
// Otherwise, it returns the integer value of the string.
    return output;
}

int main(int argc, char *argv[]) {
    // Check if there are enough arguments
    if (argc < 5)
    {
        printf("ERROR: Insufficient arguments\n");
        exit(EXIT_FAILURE);
    }

    // Convert time arguments to integers
    int hour = convert_time_string(argv[1]);
    int minute = convert_time_string(argv[2]);
    int second = convert_time_string(argv[3]);
    char *route = argv[4];

    // Get the absolute path of the script
    char fixed_route[4096];
    char *output_path = realpath(route, fixed_route);

    bool statused = false;
    int lenght_route = strlen(route);

    // Check if time arguments are valid
    if (hour == -1 || hour > 23)
    {
        printf("ERROR: Invalid hour format\n");
        statused = true;
    }
    if (minute == -1 || minute > 59)
    {
        printf("ERROR: Invalid minute format\n");
        statused = true;
    }
    if (second == -1 || second > 59)
    {
        printf("ERROR: Invalid second format\n");
        statused = true;
    }

    // Check if script path is valid
    if (lenght_route < 4 || strcmp(route + lenght_route - 3, ".sh") != 0)
    {
        printf("ERROR: Invalid route format\n");
        statused = true;
    }
    if (access(route, F_OK) == -1 || !output_path)
    {
        printf("ERROR: Path does not exist\n");
        statused = true;
    }

    // Exit program if there are any errors
    if (statused)
    {
        printf("Exit Program\n");
        exit(EXIT_FAILURE);
    }

    // Run the script at the specified time
    run_script_at_time(hour, minute, second, fixed_route);

    return 0;
}



```
